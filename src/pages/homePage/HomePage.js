import React from "react";
import {Grid} from "@material-ui/core";
import {makeStyles} from "@material-ui/core";

import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import PersonIcon from '@material-ui/icons/Person';
import DomainIcon from '@material-ui/icons/Domain';
import DoubleArrowIcon from '@material-ui/icons/DoubleArrow';
import { Link } from "react-router-dom";
import LogoUser from "../../img/logoUsers.png";


function HomePage(){

    const useStyles = makeStyles({

        main: {

            backgroundColor: '#1a867f',
            minHeight: '20vh',
            width: '100',
            paddingTop:'5px',



        },
        logoText:{
            paddingTop:'100px',
            paddingLeft:"30px",
            textAlign:'left',
            color:'white',
            fontWeight:'bold',
            fontSize:'1.5rem'
        },
        subTitleText:{
            paddingLeft:"30px",
            textAlign:'left',
            color:'white',
            fontWeight:'bold',
            fontSize:'.8rem'

        },
        listItem:{
            margin:'20px',
            textAlign:'center',
            height:'30px',
            width:'100px',

            '&:hover': {
                backgroundColor: 'red'
            }
        },
        listText:{

            fontSize:'1rem',
            color:'white',
        },
        box:{
            width: '15rem',
            height: '12rem'
        },
        userLogo:{

            textAlign:"center",
            color:'white',
           fontSize:120,
        }


    });
    const classes = useStyles();
    return(
// this is for main css
        <div >

            <Grid container align = "center" justify = "center" alignItems = "center"  >
                <Grid Item xs={12} md={12} >
                    { //<img src={Hero} style={{width:'100%',height:'100px'}}/>
                        //
                        }

                </Grid>
                <Grid Item xs={12} md={4} >
                    <img src={LogoUser} className={classes.image}/>
                    <Link to='/user'style={{textDecoration:'none'}}>
                    <Typography >

                    <Box
                        boxShadow={3}
                        bgcolor="background.paper"
                        m={1}
                        p={1}
                        fontFamily="Pacifico"
                        textAlign="center"
                        fontWeight="fontWeightBold"
                        fontStyle="italic"
                        fontSize="h3.fontSize"
                        color='green'
                        style={{ width: '20rem', height: '12rem' }}
                    >
                        <PersonIcon style={{fontSize:80}} />
                        <br />
                            USER

                    </Box>
                    </Typography>
                    </Link>

                </Grid>
                <Grid Item xs={12} md={4}>
                    <img src={LogoUser} className={classes.image}/>
                    <Link to='/domain' style={{textDecoration:'none'}}>
                    <Typography >
                        <Box
                            boxShadow={3}
                            bgcolor="background.paper"
                            m={1}
                            p={1}
                            fontFamily="Pacifico"
                            textAlign="center"
                            fontWeight="fontWeightBold"
                            fontStyle="italic"
                            fontSize="h3.fontSize"
                            color='#3faea7'
                            style={{ width: '20rem', height: '12rem' }}
                        >
                            <DomainIcon style={{fontSize:80}} />
                            <br />
                           DOMAIN

                        </Box>
                    </Typography>
                    </Link>

                </Grid>
                <Grid Item xs={12} md={4}>
                    <img src={LogoUser} className={classes.image}/>
                    <Link to='/right'style={{textDecoration:'none'}}>
                    <Typography >
                        <Box
                            boxShadow={3}
                            bgcolor="background.paper"
                            m={1}
                            p={1}
                            fontFamily="Pacifico"
                            textAlign="center"
                            fontWeight="fontWeightBold"
                            fontStyle="italic"
                            fontSize="h3.fontSize"
                            justifyContent="center"
                            alignItems="center"
                            color='#d7b816'
                            style={{ width: '20rem', height: '12rem' }}
                        >
                            <DoubleArrowIcon style={{fontSize:80}} />
                            <br />
                           Right

                        </Box>
                    </Typography>
                    </Link>
                </Grid>

            </Grid>


        </div>
    );
}
export default HomePage;