import React from 'react'


class RightApi {
    getAll = async () => {
        const response = await fetch('https://usersjava20.sensera.se/rights',
            {
                method: 'GET',
                headers: {"Content-type": "application/json"}
            });
        const result = await response.json();
        return result;

    }

    addRight = async (data = {}) => {
        const response = await fetch('https://usersjava20.sensera.se/rights',
            {
                method: 'POST',
                headers: {"Content-type": "application/json"},
                body: JSON.stringify(data)
            });
        const result = await response.json();
        return result;

    }


    updateRight = async (id, data = {}) => {
        const response = await fetch('https://usersjava20.sensera.se/rights/' + id,
            {
                method: 'PUT',
                headers: {"Content-type": "application/json"},
                body: JSON.stringify(data)
            });
        const result = await response.json();
        return result;
    }

    deleteRight = async (id) => {
        const response = await fetch('https://usersjava20.sensera.se/rights/' + id,
            {
                method: 'DELETE',
                headers: {"Content-type": "application/json"},
            });
        const result = await response.json();
        return result;
    }
    return;
}

export default new RightApi();

