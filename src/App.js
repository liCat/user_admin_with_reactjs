import './App.css';
import Header from "./myComponent/Header";
import Footer from "./myComponent/Footer";
import HomePage from "./pages/homePage/HomePage";
import DemoPage from './myComponent/DemoPage';
import ListPage from "./myComponent/ListPage";
import DomainForm from "./myComponent/DomainForm";
import UserForm from "./myComponent/UserForm";
import RightForm from "./myComponent/RightForm";
import RightUserForm from "./myComponent/RightUserForm";
import {BrowserRouter as Router, Switch, Route,} from "react-router-dom";


function App() {
    return (
        <div>

            <Router>
                <Header/>
                <div className="container m-0 p-0" style={{maxWidth: "none"}}>

                <Switch>

                    <Route  exact path="/">
                        <HomePage/>
                    </Route>

                        <Route path="/User/add">
                            <UserForm title="User" type="Add"/>
                        </Route>
                    <Route path="/addRightToUser">
                        <RightUserForm name="Add" sub="To"/>
                    </Route>
                    <Route path="/removeRightFromUser">
                        <RightUserForm name="Delete" sub="From"/>
                    </Route>

                        <Route path="/User/all">
                            <ListPage name="User"/>
                        </Route>
                        <Route path="/Domain/all">
                            <ListPage name="Domain"/>
                        </Route>
                        <Route path="/Domain/add">
                            <DomainForm title="Domain" type="Add"/>
                        </Route>
                        <Route path="/Right/add">
                            <RightForm title="Right" type="Add"/>
                        </Route>
                        <Route path="/Right/all">
                            <ListPage name="Right"/>
                        </Route>
                    <Route path="/RightUser">
                        <DemoPage name="RightUser" sub="Remove"  add="/addRightToUser" all="/removeRightFromUser"/>
                    </Route>


                    <Route path="/User">
                            <DemoPage name="User" sub="All" add="/user/add" all="/user/all"/>
                        </Route>

                        <Route path="/Domain">
                            <DemoPage name="Domain" sub="All" add="/domain/add" all="/domain/all"/>
                        </Route>

                        <Route path="/Right">
                            <DemoPage name="Right" sub="All" add="/right/add" all="/right/all"/>
                        </Route>


                    </Switch>
                </div>
                <Footer/>

            </Router>


        </div>
    );
}

export default App;
