import React, {useState} from 'react'
import {useStayl,makeStyles} from "@material-ui/core/"
import DeleteIcon from '@material-ui/icons/Delete'
import { IconButton} from "@material-ui/core"
import EditIcon from '@material-ui/icons/Edit'
import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";
import PopUpWindow from "./PopUpWindow";
import UserForm from "./UserForm";
import DomainForm from "./DomainForm";
import RightForm from "./RightForm";
import UserApi from "../api/UserApi";
import PopupWindow from "./PopUpWindow";
import DomainWithRightFormForDelete from "./DomainWithRightFormForDelete";
import Swal from "sweetalert2";



const useStyles = makeStyles((theme) => ({
    main: {

        textAlign:'center',
        padding:theme.spacing(.5),
        border:'1px',

    },
    box:{
        backgroundColor:'grey',
        margin:'0px',
        '&:hover':{
            backgroundColor:'red',

        }

    }
}));

export default function ListCard(props) {
    const {title,User}=props;

    const [openWindow, setOpenWindow] = React.useState(false);
    const [openDeleteWindow, setOpenDeleteWindow] = React.useState(false);
    const [type,setType]=useState("Edit selected user");
    const classes = useStyles();
    const closeWindow = () => {
        setOpenWindow(false);
        setOpenDeleteWindow(false);
    }
    const deleteRight = () =>{
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Not Allowed To Delete Right',
        })}

    const deleteUser = () =>{
        Swal.fire({
            title: 'Do you sure want to Delete?',
            showDenyButton: true,
            showCancelButton: true,
            confirmButtonText: `Delete`,
            denyButtonText: `Don't Delete`,
        }).then((result) => {
            if (result.isConfirmed) {
                UserApi.deleteUser(User.id).then(res => {
                    if(res.error === 'Not Found' || res.error ==='Internal Server Error'){
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: 'Not Found or Invalid',
                        })
                        console.log("no",res);}
                    else{
                        Swal.fire('Deleted!', '', 'success');
                        console.log("success",res);
                    }
                }).catch(err=> alert(err))
            } else if (result.isDenied) {
                Swal.fire('Changes are not saved', '', 'info')
            }
        })

       }




    return (
        <>
        <TableRow  hover key={User.id}>
            <TableCell component="h4" scope="row" style={{padding:'6px'}} align='center' onClick={(e)=>
            {setOpenWindow(true);
            setType("show");
            console.log(User.right);
            }}>
                {title === 'Right' ? User.key : ""}
                {title === 'Domain' ? User.domain : ""}
                {title === 'User' ? User.username : ""}
            </TableCell>
            <TableCell align="right" size='small' style={{padding:'5px'}}>
                <IconButton  aria-label="edit" style={{padding:'5px'}} onClick={(e)=>
                {setType('edit');
                setOpenWindow(true)
                }}>
                   <EditIcon  style={{fontSize:'25px', color:'orange' }}/>
                </IconButton>
            </TableCell>
            <TableCell align="right" size='small' style={{padding:'4'}}>
                <IconButton aria-label="delete"  style={{padding:'5px'}}>
                    {title === "User" ?
                        <DeleteIcon style={{fontSize:'25px', color:'red' }}  onClick= {deleteUser} /> : ""}
                    {title === "Domain" ?
                        <DeleteIcon style={{fontSize:'25px', color:'red' }}  onClick= {() => setOpenDeleteWindow(true)} /> : ""}
                    {title === "Right" ?
                        <DeleteIcon style={{fontSize:'25px', color:'red' }}  onClick= {deleteRight} />: ""}

                </IconButton>
            </TableCell>

            </TableRow>
            <PopUpWindow openWindow={openWindow} closeWindow={closeWindow}>
                {title === "User" ?
                    <UserForm title="User" obj={User} type={type}/> : ""}
                {title === "Domain" ?
                    <DomainForm title="Domain" obj={User} type={type}/> : ""}
                {title === "Right" ?
                    <RightForm title="Right" obj={User} type={type}/> : ""}

            </PopUpWindow>
            <PopupWindow openWindow={openDeleteWindow} closeWindow={closeWindow}>
                { User === null ? <div> Loading....</div>:
                <DomainWithRightFormForDelete name={User.domain} obj={User.rights} id={User.id}/>}
            </PopupWindow>
        </>



    )
}
