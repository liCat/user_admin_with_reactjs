import React, { useState } from 'react'
import { Grid, } from '@material-ui/core';
import Paper from "@material-ui/core/Paper";
import makeStyles from "@material-ui/core/styles/makeStyles";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import DomainApi from "../api/DomainApi";
import PersonIcon from "@material-ui/icons/Person";
import {blueGrey, yellow} from "@material-ui/core/colors";
import * as Swal from "sweetalert2";

const useStyles = makeStyles((theme) => ({
    main:{
        minHeight:"68vh",
    },
    root: {
        padding:'20px',
        alignContent:'center',
        textAlign:'center',
        minHeight:"15vh",


    },
    ami:{
        display:"flex",
        alignItems:'center',
        justifyContent:'center',
        textAlign:'center',
        margin:'30px',

    },

    form:{
        '& .MuiTextField-root': {
            margin: theme.spacing(1),
            width: '25ch',
        },
    }
}));


export default function DomainForm(props) {
    const classes = useStyles();
    const {id,title, obj,type}=props;
    const [name,setName]=useState("");
    const [view, setView]=useState(true);
    const [nameErrors, setNameErrors]=useState({name:''});

    const handleNameChange=(e)=>{
        const newName=e.target.value;
        nameValidation(newName);
        setName(newName);


    }
    const nameValidation=(name)=>{
        let pat=/^[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-]+$/u;
        if((!pat.test(name)) || name.length<=4 || name.length>=10){
            setNameErrors({name:'Invalid Name'});
        } else
            setNameErrors({name:''});
    }


    const handleSubmitSave=(e)=>{
        e.preventDefault();
        if(name === ''){
           setNameErrors({name: ' field blank'})
        }
        else if(nameErrors.name === '') {
                Swal.fire({
                    title: 'Do you want to save the changes?',
                    showDenyButton: true,
                    showCancelButton: true,
                    confirmButtonText: `Save`,
                    denyButtonText: `Don't save`,
                }).then((result) => {
                    /* Read more about isConfirmed, isDenied below */
                    if (result.isConfirmed) {
                        DomainApi.addDomain({domain: name}).then(res => { Swal.fire('Saved!', '', 'success')});

                    } else if (result.isDenied) {
                        Swal.fire('Changes are not saved', '', 'info')
                    }
                    window.location.href="/domain/all";
                })
               // window.location.href="/";


        }else
        {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Something went wrong!',
            })
        }
        setName('');
    }

    const handleSubmitEdit = (e) => {
        e.preventDefault();
        if(name === ''){
            setNameErrors({name: ' field blank'})
        } else if(nameErrors.name === '') {
                    DomainApi.updateDomain(obj.id,{domain: name}).then(res => alert("Updated"));

        } else
        {
            alert("Error");
        }
        window.location.href="/domain/all";
        setName('');

    }
  /*  const InputDomainName = () => (
        <TextField
            id="outlined-name-input"
            label="Domain Name"
            value={obj.domain}
            variant="outlined"
            InputProps={{
                readOnly: true,
            }}/>
    )
    const EditDomainName = () => (
        <TextField
            id="outlined-name-input"
            label={props.title}
            defaultValue={obj.domain}
            variant="outlined"
            style={{width:"110%"}}
            error={Boolean(nameErrors?.name)}
            helperText={(nameErrors?.name)}
            onChange={handleNameChange}
        />
    )
    const SaveDomainName = () => (
        <TextField
            id="outlined-name-input"
            label="DomainName"
            value={name}
            variant="outlined"
            style={{width:"110%"}}
            error={Boolean(nameErrors?.name)}
            helperText={(nameErrors?.name)}
            required
            onChange={handleNameChange}
        />
    )
    const SelectDomainNameInput = () => {
        switch (type) {
            case "show": return (<InputDomainName/>)
            case "edit": return (<EditDomainName/>)
            default: return (<SaveDomainName/>)
        }
    }*/

    return (
        <div className={classes.main}>
            <div>
                <Typography variant="h1">
                    {props.type} {props.title}
                </Typography>
            </div>
            <div  className={classes.ami}>
                <Paper elevation={20} className={classes.root} >
                    <form className={classes.form} noValidate autoComplete="off" >
                        <Grid item>
                            <PersonIcon style={{ fontSize: 100, color: blueGrey[500] }} />
                        </Grid>
                        <Grid container>
                            <Grid item xs={10}>
                                {type === 'show' ?
                                <TextField
                                    id="outlined-name-input"
                                    label="Domain Name"
                                    value={obj.domain}
                                    variant="outlined"
                                    InputProps={{
                                        readOnly: true,
                                    }}/>:
                                type === 'edit' ?
                                <TextField
                                    id="outlined-name-input"
                                    label={props.title}
                                    defaultValue={obj.domain}
                                    variant="outlined"
                                    style={{width:"110%"}}
                                    error={Boolean(nameErrors?.name)}
                                    helperText={(nameErrors?.name)}
                                    onChange={handleNameChange}
                                />:
                                <TextField
                                    id="outlined-name-input"
                                    label="DomainName"
                                    value={name}
                                    variant="outlined"
                                    style={{width:"110%"}}
                                    error={Boolean(nameErrors?.name)}
                                    helperText={(nameErrors?.name)}
                                    required
                                    onChange={handleNameChange}
                                />}
                            </Grid>
                        </Grid>
                        <div>

                            { type==="show" ? "":
                                type==="edit" ?
                                    <Button type="submit" size="large" variant="contained" color="primary" onClick={handleSubmitEdit}>
                                        Edit

                                    </Button>:
                                    <Button type="submit" size="large" variant="contained" color="primary" onClick={handleSubmitSave}>
                                        Save

                                    </Button>
                            }


                        </div>

                    </form>

                </Paper>
            </div>

        </div>


    )
}