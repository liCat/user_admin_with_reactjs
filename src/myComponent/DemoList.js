import React from "react";
import {makeStyles} from "@material-ui/core/"
import List from "@material-ui/core/List"
import ListItem from "@material-ui/core/ListItem"
import ListItemText from "@material-ui/core/ListItemText"
import DeleteIcon from '@material-ui/icons/Delete'
import { IconButton, ListItemSecondaryAction} from "@material-ui/core"
import EditIcon from '@material-ui/icons/Edit'
import PersonIcon from "@material-ui/icons/Person";
import  SearchIcon from "@material-ui/icons/Search";

const useStyles = makeStyles((theme) => ({
    root: {
        backgroundColor:'blacklist',
        textColor:'wait',
        textAlign:'center',
        padding: "40px 60px",
        width:300,
        alignItems: 'center',
        display:'center list',
    },

    input: {
        marginLeft: theme.spacing(1),
        flex: 1,
    },
    iconButton: {
        padding: 10,
    },

}));
const People =[
    {
        id: 'Victor',
        password:'',
    },
    {
        id: 'Ketty',
        password:'',
    },
    {
        id: 'Filip',
        password:'',
    },
    {
        id: 'Victoria',
        password:'',

    },
    {
        id: 'Tom',
        password:'',

    },
];

export default function DemoList() {
    const classes = useStyles();
        const [SearchUser, SetSearch] = React.useState("");
        const [SearchResults, SetSearchResults] = React.useState([]);
        const handleChange = event => {
            SetSearch(event.target.value);


            const userItem = People.map((People) =>
                <li key={People.id}>
                    {People.password}
                </li>
            );

        React.UseEffect( userItem => {
            const results = People.filter(person =>
                person.toString().includes(SearchUser)
            );
            SetSearchResults(results);
        }, [ SearchUser]);

        return(
                <div>
                <input
                    type="text"
                    placeholder="Search user.."
                    value={SearchUser}
                    onChange={handleChange}
                />
                    <SearchIcon/>
                <ul>
                    {SearchResults.map(userItem =>(
                        <li>{userItem}</li>
                    ))}
                </ul>

        <List component="nav" className={classes.root} aria-label="contacts">
            <h1> User List</h1>
            <ListItem button>
                <PersonIcon/>
                <ListItemText inset primary="Victor Kalkili" />
                <IconButton  aria-label="edit">
                    <EditIcon />
                </IconButton>
                        <ListItemSecondaryAction>
                    <IconButton aria-label="delete">
                        <DeleteIcon />
                    </IconButton>
                </ListItemSecondaryAction>
                </ListItem>

            <ListItem button>
                <PersonIcon/>
                <ListItemText inset primary="Ketty Anderson" />
                <IconButton  aria-label="edit">
                    <EditIcon/>
                </IconButton>
                <ListItemSecondaryAction>
                    <IconButton aria-label="delete">
                    <DeleteIcon />
                </IconButton>
                </ListItemSecondaryAction>
            </ListItem>

            <ListItem button>
                <PersonIcon/>
                <ListItemText inset primary="Filip Peterson" />
                <IconButton  aria-label="edit">
                    <EditIcon/>
                </IconButton>
                <ListItemSecondaryAction>
                    <IconButton aria-label="delete">
                        <DeleteIcon />
                    </IconButton>
                </ListItemSecondaryAction>
            </ListItem>

            <ListItem button>
                <PersonIcon/>
                <ListItemText inset primary="Victoria Secret" />
                <IconButton  aria-label="edit">
                    <EditIcon/>
                </IconButton>
                <ListItemSecondaryAction>
                    <IconButton aria-label="delete">
                        <DeleteIcon />
                    </IconButton>
                </ListItemSecondaryAction>
            </ListItem>

            <ListItem button>
                <PersonIcon/>
                <ListItemText inset primary="Tom Marcony" />
                <IconButton  aria-label="edit">
                    <EditIcon/>
                </IconButton>
                <ListItemSecondaryAction>
                    <IconButton aria-label="delete">
                        <DeleteIcon />
                    </IconButton>
                </ListItemSecondaryAction>
            </ListItem>

            <ListItem button>
                <PersonIcon/>
                <ListItemText inset primary="Eric Hoffman" />
                <IconButton  aria-label="edit">
                    <EditIcon/>
                </IconButton>
                <ListItemSecondaryAction>
                    <IconButton aria-label="delete">
                        <DeleteIcon />
                    </IconButton>
                </ListItemSecondaryAction>
            </ListItem>
        </List>
        </div>
        )
        }}