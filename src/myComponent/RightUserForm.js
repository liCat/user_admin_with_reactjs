import React, {useEffect, useState} from "react";
import makeStyles from "@material-ui/core/styles/makeStyles";
import Typography from "@material-ui/core/Typography";
import {FormControl, Grid, Paper, Select, TextField} from "@material-ui/core";
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';
import Button from "@material-ui/core/Button";
import DomainApi from "../api/DomainApi";
import UserApi from "../api/UserApi";
import Swal from "sweetalert2";


const useStyles = makeStyles((theme) => ({
    root: {
        padding:'20px',
        alignContent:'center',
        textAlign:'center',
        minHeight:"20vh",
    },
    formControl: {
        margin: theme.spacing(2),
        minWidth: "60vh",
    },
    form:{
        '& .MuiTextField-root': {
            margin: theme.spacing(1),
            width: '25ch',
        },
        button:{
            textAlign:"center",
        }
    }
}));

export default function RightUserForm(props) {
    const classes = useStyles();
    const [right, setRight]=useState("");
    const [rights, setRights]=useState([]);
    const [domains, setDomains]=useState([]);
    const [user, setUser]=useState("");
    const [users, setUsers]=useState([]);
    const [loading, setLoading]=useState(false);
    const [domain, setDomain]= useState("");

    const handleSelectChangeRight=(e)=>{

        setRight(e.target.value);
    }
    const handleSelectChangeDomain=(e)=>{
        console.log(e.target.value);
        setDomain(e.target.value);
        DomainApi.findDomain(e.target.value).then(res=>{
            // setRights(res.rights);
            console.log(res[0].rights);
            setRights(res[0].rights);

        }).catch(err=> console.log(err));

    }
  /*  const getRelatedRights=()=>{
        DomainApi.findDomain(domain).then(res=>{
            // setRights(res.rights);
            setRights(res[0].rights);

        }).catch(err=> console.log(err));

    }*/


    const handleSelectChangeUser=(e)=>{
       // console.log(e.target.value);
        setUser(e.target.value);
    }
    const handleDeleteClick=()=>{
        if (user !== '' && domain !== '' && right !== '') {
            Swal.fire({
                title: 'Do you sure want to Delete?',
                showDenyButton: true,
                showCancelButton: true,
                confirmButtonText: `Delete`,
                denyButtonText: `Don't Delete`,
            }).then((result) => {
                if (result.isConfirmed) {
                    UserApi.removeRightFromUser({username: user, right: right, domain:domain}).then(res => {
                        if(res.error === 'Not Found' || res.error ==='Internal Server Error'){
                            Swal.fire({
                                icon: 'error',
                                title: 'Oops...',
                                text: 'Not Found or Invalid',
                            })
                            console.log("no",res);}
                        else{
                            Swal.fire('Deleted!', '', 'success');
                            console.log("success",res);
                        }
                    }).catch(err=> alert(err))
                } else if (result.isDenied) {
                    Swal.fire('Changes are not saved', '', 'info')
                }
            })
        } else
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Invalid Input',
            })
    }
    const handleSaveClick=()=>{
        if (user !== '' && domain !== '' && right !== '') {
            Swal.fire({
                title: 'Do you sure want to Add?',
                showDenyButton: true,
                showCancelButton: true,
                confirmButtonText: `Add`,
                denyButtonText: `Don't add`,
            }).then((result) => {
                if (result.isConfirmed) {
                    UserApi.addRightsToUser({username: user, right: right, domain:domain}).then(res => {
                        if(res.error === 'Not Found' || res.error ==='Internal Server Error'){
                            Swal.fire({
                                icon: 'error',
                                title: 'Oops...',
                                text: 'Not Found or Invalid',
                            })
                            console.log("no",res);}
                        else{
                            Swal.fire('Saved!', '', 'success');
                            console.log("success",res);
                        }
                    }).catch(err=> alert(err))
                } else if (result.isDenied) {
                    Swal.fire('Changes are not saved', '', 'info')
                }
            })
        } else
           Swal.fire({
               icon: 'error',
               title: 'Oops...',
               text: 'Invalid Input',
           })
    }
    useEffect(async () => {

            UserApi.getAll().then(res => {

                setUsers(res);
                setLoading(true);

            }).catch(err=> console.log(err));

        DomainApi.getAll().then(res => {

            setDomains(res);
            setLoading(true);
        }).catch(err=> console.log(err));

    },[]);

        if(loading === false)
        return <div>loading</div>
        else
            return (
        <div className={classes.root}>

            <Paper  elevation={20} className={classes.root}>
                <Typography align="center" variant="h3">
                    {props.name} Rights {props.sub} Users
                </Typography>

                <form className={classes.form}>
                    <Grid container>
                        <Grid Item xs={12}  >
                            <FormControl variant="outlined" className={classes.formControl}>
                            <InputLabel> User</InputLabel>
                            <Select
                                label="User"
                                value={user}
                                onChange={handleSelectChangeUser}
                                displayEmpty>
                                <MenuItem value="">None</MenuItem>
                                {(users.map(
                                        user =>
                                            (<MenuItem key={user.id} value={user.username}>{user.username}</MenuItem>)))
                                }


                            </Select>
                                </FormControl>
                        </Grid>

                        <Grid Item xs={12}  >
                            <FormControl variant="outlined" className={classes.formControl}>
                            <InputLabel>Domain</InputLabel>
                            <Select
                                label="Domain"
                                value={domain}
                                onChange={handleSelectChangeDomain}
                                displayEmpty>
                                <MenuItem value="">None</MenuItem>
                                {

                                    domains.map(
                                        domain =>
                                            (<MenuItem key={ domain.id} name={domain.rights[0]} value={domain.domain}>{domain.domain}</MenuItem>)
                                    )}


                            </Select>
                            </FormControl>
                        </Grid>

                        <Grid Item xs={12}>
                            <FormControl variant="outlined" className={classes.formControl}>
                                <InputLabel>Right</InputLabel>
                                <Select
                                    label="Right"
                                    value={right}
                                    onChange={handleSelectChangeRight}
                                    displayEmpty>
                                    <MenuItem value="">None</MenuItem>
                                    {

                                        rights.map(
                                            right =>
                                                (<MenuItem key={ right.index} value={ right}>{ right}</MenuItem>)
                                        )}


                                </Select>
                            </FormControl>
                        </Grid>

                    </Grid>
                    <div>
                        {props.name === "Add" ?
                            <Button size="large" variant="contained" color="primary" onClick={handleSaveClick}>
                                Save
                            </Button>:
                            <Button size="large" variant="contained" color="primary" onClick={handleDeleteClick}>
                                Delete
                            </Button>
                        }
                    </div>
                </form>
            </Paper>
        </div>
    )
}