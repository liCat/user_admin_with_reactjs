import React, { useState, useEffect } from 'react'
import { Grid, } from '@material-ui/core';
import Paper from "@material-ui/core/Paper";
import makeStyles from "@material-ui/core/styles/makeStyles";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import UserApi from '../api/UserApi';
import PersonIcon from "@material-ui/icons/Person";
import {blueGrey} from "@material-ui/core/colors";
import * as Swal from "sweetalert2";



const useStyles = makeStyles((theme) => ({
    root: {
        padding:'20px',
        alignContent:'center',
        textAlign:'center',
        minHeight:"40vh",


    },
    ami:{
        display:"flex",
        alignItems:'center',
        justifyContent:'center',
        textAlign:'center',
        margin:'30px',

    },

    form:{
        '& .MuiTextField-root': {
            margin: theme.spacing(1),
            width: '25ch',
        },
    }
}));


export default function UserForm(props) {

    const classes = useStyles();
    const {title, obj,type,view}=props;
    const [name,setName]=useState("");
    const [password,setPassword]=useState("");
    const [passwordSalt,setPasswordSalt]=useState("");
    const [nameErrors, setNameErrors]=useState({name:''});
    const [passwordErrors, setPasswordErrors]=useState({name:''});
    const [passwordSaltErrors,setPasswordSaltErrors]=useState({name:''});

    const handleNameChange=(e)=>{
        let newName = e.target.value;
        nameValidation(newName);
        setName(newName);

    }
    const nameValidation=(name)=>{
        let pat=/^[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-]+$/u;

        if((!pat.test(name)) || name.length<=4 || name.length>=10){
            setNameErrors({name:'Invalid Name'});
        }
        else
            setNameErrors({name:''});
    }
    const handlePasswordChange=(e)=>{
       const pass =e.target.value;
       passwordValidation(pass);
        setPassword(pass);

    }

    const passwordValidation=(password)=>{
        let pat=/^(?=.*([A-Z]){1,})(?=.*[!@#$&*]{1,})(?=.*[0-9]{1,})(?=.*[a-z]{1,}).{7,100}$/;

        if(!pat.test(password)) {
            setPasswordErrors({name:'Invalid. one Cap 3small 1spec 1no 8c '});
            setPasswordSaltErrors({name:'Invalid. one Cap 3small 1spec 1no 8c '});
        }
        else {
            setPasswordErrors({name: ''});
            setPasswordSaltErrors({name: ''});
        }
    }

    const handlePasswordSaltChange = (e) => {
        setPasswordSalt(e.target.value);
        passwordSaltValidation();

    }
    const passwordSaltValidation=()=>{
        let pat=/^(?=.*([A-Z]){1,})(?=.*[!@#$&*]{1,})(?=.*[0-9]{1,})(?=.*[a-z]{1,}).{7,100}$/;

        if(!pat.test(passwordSalt)){

            setPasswordSaltErrors({name:'Invalid. one Cap 3small 1spec 1no 8c '});
        }
        else {

            setPasswordSaltErrors({name: ''});
        }
    }


    const handleSubmit=(e)=>{
        e.preventDefault()

        if (name === '' || password === '') {

            setNameErrors({name: "Invalid, Blank"});
            setPasswordErrors({name: "Invalid, Blank"});

        } else if (nameErrors.name === '' && passwordErrors.name === '' && passwordSaltErrors.name === '') {
            Swal.fire({
                title: 'Do you sure want to Add?',
                showDenyButton: true,
                showCancelButton: true,
                confirmButtonText: `Add`,
                denyButtonText: `Don't add`,
            }).then((result) => {
                if (result.isConfirmed) {
                    UserApi.addUser({
                        username: name,
                        password: password
                    }).then(res => {
                        if(res.error === 'Not Found'|| res.error ==='Internal Server Error')
                            Swal.fire({
                                icon: 'error',
                                title: 'Oops...',
                                text: 'Not Found or Server Error',
                            })
                        else{
                            Swal.fire('Saved!', '', 'success');

                        }

                    }).catch(err=> alert(err))


                } else if (result.isDenied) {
                    Swal.fire('Changes are not saved', '', 'info')

                }

            })

        } else
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Server Error',
            })
        console.log(nameErrors.name);
        console.log(passwordErrors.name);
        setPasswordSalt('');
        setPassword('');
        setName('');
    }
    const handleEdit=(e)=>{
        e.preventDefault();
        if (name === '' || password === '') {
            setNameErrors({name: ' field blank'})
        } else if(nameErrors.name === '' && passwordErrors.name === ''){
            UserApi.updateUser(obj.id,{domain: name}).then(res => alert("Updated"));

        } else
        {
            alert("Error");
        }
        window.location.href="/user/all";
        setName('');
        setPassword('');
    }

useEffect(()=>{


    })




    return (
        <>
            <div>
                <Typography variant="h1">
                    {type} {title}
                </Typography>
            </div>
            <div  className={classes.ami}>
                <Paper elevation={20} className={classes.root} >
                    <form className={classes.form} noValidate autoComplete="off" >
                        <Grid item>
                            <PersonIcon style={{ fontSize: 100, color: blueGrey[500] }} />
                        </Grid>
                        <Grid container>

                            <Grid item xs={10} >
                                {type==="show" ?

                                    <TextField

                                        id="outlined-name-input"
                                        label={props.title}
                                        value={obj.username}
                                        variant="outlined"

                                        InputProps={{
                                            readOnly: true,
                                        }} />:

                                    type==="edit"?

                                    <TextField
                                    id="outlined-name-input"
                                    label={props.title}
                                    defaultValue={obj.username}

                                    variant="outlined"
                                    style={{width:"115%"}}
                                    error={Boolean(nameErrors?.name)}
                                    helperText={(nameErrors?.name)}

                                    onChange={handleNameChange}

                                 />:
                                        <TextField
                                            id="outlined-name-input"
                                            label="UserName"
                                            value={name}
                                            variant="outlined"
                                            style={{width:"115%"}}
                                            error={Boolean(nameErrors?.name)}
                                            helperText={(nameErrors?.name)}
                                            required
                                            onChange={handleNameChange}

                                        />
                                }
                            </Grid>
                            <Grid item xs={10}>
                                {type==="show" ?
                                <TextField
                                    id="outlined-password-input"
                                    label='password'
                                    value={obj.password}
                                    type="password"
                                    variant="outlined"
                                    InputProps={{
                                        readOnly: true,
                                    }}
                                />:
                                type==='edit' ?
                                    <TextField
                                        id="outlined-password-input"
                                        label="password"
                                        defaultValue={obj.password}
                                        type="password"
                                        variant="outlined"
                                        error={Boolean(passwordErrors?.name)}
                                        helperText={(passwordErrors?.name)}
                                        style={{width:"115%"}}

                                        onChange={handlePasswordChange}

                                    />:
                                    <TextField
                                        id="outlined-password-input"
                                        label="Password"
                                        value={password}
                                        type="password"
                                        variant="outlined"
                                        error={Boolean(passwordErrors?.name)}
                                        helperText={(passwordErrors?.name)}
                                        style={{width:"115%"}}
                                        required
                                        onChange={handlePasswordChange}
                                        />
                                }
                            </Grid>

                        </Grid>
                        <div>

                            { type==="show" ? "":
                                type==="edit" ?
                                    <Button type="submit" size="large" variant="contained" color="primary" onClick={handleEdit}>
                                        Edit

                                    </Button>:
                                    <Button type="submit" size="large" variant="contained" color="primary" onClick={handleSubmit}>
                                        Save

                                    </Button>


                            }


                        </div>

                    </form>

                </Paper>
            </div>

        </>


    )
}