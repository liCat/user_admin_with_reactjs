import React, { useState } from 'react'
import { Grid, } from '@material-ui/core';
import Paper from "@material-ui/core/Paper";
import makeStyles from "@material-ui/core/styles/makeStyles";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import RightApi from "../api/RightApi";
import {blueGrey} from "@material-ui/core/colors";
import PersonIcon from "@material-ui/icons/Person";
import Swal from "sweetalert2";


const useStyles = makeStyles((theme) => ({
    root: {
        padding:'20px',
        alignContent:'center',
        textAlign:'center',
        minHeight:"20vh",


    },
    ami:{
        display:"flex",
        alignItems:'center',
        justifyContent:'center',
        textAlign:'center',
        margin:'30px',

    },

    form:{
        '& .MuiTextField-root': {
            margin: theme.spacing(1),
            width: '25ch',
        },
    }
}));


export default function DomainForm(props) {
    const classes = useStyles();
    const {title, obj,type}=props;
    const [name,setName]=useState("");
    const [domainName,setDomainName]=useState("");
    const [nameErrors, setNameErrors]=useState({name:''});
    const [domainNameErrors,setDomainNameErrors]=useState({name:''});

    const handleNameChange=(e)=>{
        const newName=e.target.value;
        nameValidation(newName);
        setName(newName);


    }
    const nameValidation=(name)=>{
        let pat=/^[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-]+$/u;

        if((!pat.test(name)) || name.length<=4 || name.length>=10){
            setNameErrors({name:'Invalid Name'});
        } else
            setNameErrors({name:''});
    }

    const handleDomainNameChange=(e)=>{
        const newName=e.target.value;
        domainNameValidation(newName);
        //setName(newName);
        setDomainName(newName);


    }
    const domainNameValidation=(domainName)=>{
        let pat=/^[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-]+$/u;

        if((!pat.test(domainName)) && (domainName.length<=4) && (domainName.length>=10)){
            setDomainNameErrors({name:'Invalid Name'});
        }
        else
            setDomainNameErrors({name:''});
    }




    const handleSubmit=(e)=>{
        e.preventDefault()
        if (name === '' || domainName === '') {
            setNameErrors({name: "Invalid, Blank"});
            setDomainNameErrors({name: "Invalid, Blank"});

        } else if (nameErrors.name === '' && domainNameErrors.name === '') {
            Swal.fire({
                title: 'Do you sure want to Add?',
                showDenyButton: true,
                showCancelButton: true,
                confirmButtonText: `Add`,
                denyButtonText: `Don't add`,
            }).then((result) => {
                if (result.isConfirmed) {
                    RightApi.addRight({key: name, domain: domainName}).then(res => {
                        if(res.error === 'Not Found')
                            Swal.fire({
                                icon: 'error',
                                title: 'Oops...',
                                text: 'Not Found',
                            })
                        else{
                            Swal.fire('Saved!', '', 'success');

                        }

                    }).catch(err=> alert(err))


                } else if (result.isDenied) {
                    Swal.fire('Changes are not saved', '', 'info')

                }

            })
        } else
            alert("error");

        setName('');
        setDomainName('');

    }
    const handleEdit=(e)=>{
        alert("Not Allowed to Edit");
    }


    return (
        <>
            <div>
                <Typography variant="h1">
                    {type} {title}
                </Typography>
            </div>
            <div  className={classes.ami}>
                <Paper elevation={20} className={classes.root} >
                    <form className={classes.form} noValidate autoComplete="off" >
                        <Grid item>
                            <PersonIcon style={{ fontSize: 100, color: blueGrey[500] }} />
                        </Grid>
                        <Grid container>
                            <Grid Item xs={10} >
                                {type==="show" ?
                                    <TextField
                                        id="outlined-name-input"
                                        label={props.title}
                                        value={obj.key}
                                        variant="outlined"

                                        InputProps={{
                                            readOnly: true,
                                        }} />:
                                    type==="edit"?
                                        <TextField
                                            id="outlined-name-input"
                                            label="name"
                                            defaultValue={obj.key}

                                            variant="outlined"
                                            style={{width:"115%"}}
                                            error={Boolean(nameErrors?.name)}
                                            helperText={(nameErrors?.name)}

                                            onChange={handleNameChange}

                                        />:
                                        <TextField
                                            id="outlined-name-input"
                                            label="UserRights"
                                            value={name}
                                            variant="outlined"
                                            style={{width:"115%"}}
                                            error={Boolean(nameErrors?.name)}
                                            helperText={(nameErrors?.name)}
                                            required
                                            onChange={handleNameChange}

                                        />
                                }
                            </Grid>

                            <Grid Item xs={10} >
                                {type==="show" ?
                                    <TextField
                                        id="outlined-domain-input"
                                        label="DomainName"
                                        value={obj.domain}
                                        variant="outlined"

                                        InputProps={{
                                            readOnly: true,
                                        }} />:
                                    type==="edit"?
                                        <TextField
                                            id="outlined-domain-input"
                                            label="DomainName"
                                            defaultValue={obj.domain}

                                            variant="outlined"
                                            style={{width:"115%"}}
                                            error={Boolean(domainNameErrors?.name)}
                                            helperText={(domainNameErrors?.name)}

                                            onChange={handleDomainNameChange}

                                        />:
                                        <TextField
                                            id="outlined-domain-input"
                                            label="DomainName"
                                            value={domainName}
                                            variant="outlined"
                                            style={{width:"115%"}}
                                            error={Boolean(domainNameErrors?.name)}
                                            helperText={(domainNameErrors?.name)}
                                            required
                                            onChange={handleDomainNameChange}

                                        />
                                }
                            </Grid>


                        </Grid>

                        <div>

                            { type==="show" ? "":
                                type==="edit" ?
                                    <Button type="submit" size="large" variant="contained" color="primary"
                                            onClick={handleEdit}>
                                        Edit

                                    </Button>:
                                    <Button type="submit" size="large" variant="contained" color="primary"
                                            onClick={handleSubmit}>
                                        Save

                                    </Button>


                            }


                        </div>

                    </form>

                </Paper>
            </div>

        </>


    )
}