import React, { useState } from 'react'
import { makeStyles } from "@material-ui/core";

export function FormDemo(initialFValues) {


    const [values, setValues] = useState(initialFValues);

    const handleInputChange = e => {
        const { name, value } = e.target
        setValues({
            ...values,
            [name]: value
        })
    }

    return {
        values,
        setValues,
        handleInputChange,
    }
}


const useStyles = makeStyles(theme => ({
    root: {
        '& .MuiFormControl-root': {
            width: '100%',
            margin: theme.spacing(2)
        }
    }
}))

export function NewForm(props) {

    const classes = useStyles();
    const {children} = props;
    return (

        <form className={classes.root} autoComplete="off">
            {props.children}
        </form>
    )
}