import React, {useState} from "react";
import makeStyles from "@material-ui/core/styles/makeStyles";
import Typography from "@material-ui/core/Typography";
import {FormControl, Grid, Paper, Select, TextField} from "@material-ui/core";
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';
import Button from "@material-ui/core/Button";
import DomainApi from "../api/DomainApi";


const useStyles = makeStyles((theme) => ({
    root: {
        padding:'20px',
        alignContent:'center',
        textAlign:'center',
        minHeight:"20vh",



    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
    },
    form:{
        '& .MuiTextField-root': {
            margin: theme.spacing(1),
            width: '25ch',
        },
    }
}));

export default function DomainWithRightFormForDelete (props) {
    const classes = useStyles();
    const {id,name,obj}=props;
    const [right, setRight]=useState("");
    const handleSelectChange=(e)=>{
        console.log(e.target.value);
        setRight(e.target.value);

    }
    const handleDeleteClick=()=>{
        if(right===""){
            DomainApi.deleteDomain(id).then(res=>
                console.log("Conferm Delete Only Domain")
            )
        }
      DomainApi.deleteDomainWithRight(id,right).then(res=>
          console.log("Conferm Delete Rights of Domain")
      )
    }
    return (
        <div className={classes.root}>

        <Paper  elevation={20} className={classes.root}>

                <Typography align="center">
                    Delete Domain
                </Typography>

            <form className={classes.form}>
                <Grid container>
                    <Grid Item xs={12}  >
                <TextField
                    id="outlined-name-input"
                    label="Domain name"
                    value={props.name}
                    variant="outlined"

                    InputProps={{
                        readOnly: true,
                    }} >

                </TextField>
                        </Grid>
                    <Grid Item xs={12}>
                        <FormControl variant="outlined" className={classes.formControl}>
                            <InputLabel> </InputLabel>
                            <Select

                                label="Rights"
                                value={right}
                                onChange={handleSelectChange}
                                displayEmpty
                            >
                                <MenuItem value="">None</MenuItem>
                                {

                                    obj.map(
                                        item =>

                                            (<MenuItem key={item.index} value={item}>{item}</MenuItem>)

                                    )
                                }


                            </Select>

                        </FormControl>
</Grid>
</Grid>
                <div>


                            <Button type="submit" size="large" variant="contained" color="primary" onClick={handleDeleteClick}>
                               Delete

                            </Button>





                </div>
</form>
</Paper>
</div>
)
}